/*
 * Common API for all pages
 */
REST_API_PREFIX="/symdb/"
function fnGetProductRestPrefix() {
	var product = $.url().param('product');
	var version = $.url().param('version');

	if (!product) {
		product = "ohos_rk3568";
		version = "3.2.6.5";
	}
	return REST_API_PREFIX + product + "/" + version + "/";
}

function redirectToDefaultProduct() {
	$.get(REST_API_PREFIX + "products", function (data, status) {
		var products = JSON.parse(data)["data"];

		window.location = fnGetStaticHtmlUrl("modules", products[0][1], products[0][2]);
	});
}

var tableOptions = {
	"deps_dialog": {
		"title": "依赖关系列表",
		"buttons": [{
			"id": "download_xml_deps", "name": "XML下载"
		}, {
			"id": "download_svg_deps", "name": "SVG下载"
		}, {
			"id": "download_png_deps", "name": "PNG下载"
		}, {
			"id": "download_dot_deps", "name": "GraphViz文件下载"
		}],
		"table": {
			"id": "depends",
			"cols": ["Id", "CallerId", "CalleeId", "调用模块", "被调用模块", "调用次数"]
		}
	},
	"help_info_dialog": {
		"title": "帮助信息",
		"extra": {
			"class": "help_modal",
			"style": "transform: translateX(-50%) translateY(-50%)"
		},
		"wrapper": {
			"id": "help_info_img",
			"type": "img"
		}
	},
	"module_dialog": {
		"title": "模块详细信息",
		"wrapper": {
			"id": "accordion",
			"type": "div"
		}
	},
	"callinfo_dialog": {
		"title": "调用详情",
		"buttons": [{
			"id": "download_xml_callinfo", "name": "XML下载"
		}],
		"table": {
			"id": "callinfo_table",
			"cols": ["调用者", "提供者", "函数名", "类型", "段名称"]
		}
	},
	"symbol_dialog": {
		"title": "符号信息",
		"buttons": [{
			"id": "download_xml_symbols", "name": "XML下载"
		}],
		"table": {
			"id": "symbol_table",
			"cols": ["调用者", "提供者", "函数名", "类型", "段名称"]
		}
	},
	"processes_dialog": {
		"title": "进程信息",
		"buttons": [{
			"id": "download_xml_processes", "name": "XML下载"
		}],
		"table": {
			"id": "processes_table",
			"cols": ["id", "record_id", "record_name", "pid", "ppid", "command", "threads", "rss", "vsize", "region_types", "vmmap_regions", "vmmap_vsize", "vmmap_resident", "vmmap_dirty", "vmmap_swap", "footprint_Dirty", "footprint_Clean", "footprint_Reclaimable", "footprint_Regions", "cpu", "state", "priority", "nice", "wq"]
		}
	},
	"processes_by_name_dialog": {
		"title": "进程内存变化",
		"buttons": [{
			"id": "download_xml_processes", "name": "XML下载"
		}],
		"table": {
			"id": "processes_by_name_table",
			"cols": ["id", "record_id", "record_name", "pid", "ppid", "command", "threads", "rss", "vsize", "region_types", "vmmap_regions", "vmmap_vsize", "vmmap_resident", "vmmap_dirty", "vmmap_swap", "footprint_Dirty", "footprint_Clean", "footprint_Reclaimable", "footprint_Regions", "cpu", "state", "priority", "nice", "wq"]
		}
	},
	"region_types_dialog": {
		"title": "内存页分类信息",
		"buttons": [{
			"id": "download_xml_region_types", "name": "XML下载"
		}],
		"table": {
			"id": "region_types_table",
			"cols": ["id", "record_id", "processes", "record_name", "name", "regions", "vsize", "resident", "dirty", "swap"]
		}
	},
	"region_details_dialog": {
		"title": "内存页详细信息",
		"buttons": [{
			"id": "download_xml_region_details", "name": "XML下载"
		}],
		"table": {
			"id": "region_details_table",
			"cols": ["id", "record_id", "process_id", "record_name", "command", "type", "addr", "ugo", "sharemode", "purge", "detail", "vsize", "resident", "dirty", "swap"]
		}
	},
	"vmas_dialog": {
		"title": "VMA信息",
		"buttons": [{
			"id": "download_xml_vmas", "name": "XML下载"
		}],
		"table": {
			"id": "vmas_table",
			"cols": ["ID", "Process", "Name", "Start", "End", "Permission", "Offset", "Dev", "Idx", "Size", "Rss", "Pss", "Swap", "SwapPss", "Shared_Clean", "Shared_Dirty", "Private_Clean", "Private_Dirty", "Referenced", "Anonymous", "LazyFree", "KernelPageSize", "MMUPageSize", "AnonHugePages", "ShmemPmdMapped", "Shared_Hugetlb", "Private_Hugetlb", "Locked", "THPeligible", "FilePmdMapped"]
		}
	},
	"objects_dialog": {
		"title": "加载对象信息",
		"buttons": [{
			"id": "download_xml_objects", "name": "XML下载"
		}],
		"table": {
			"id": "objects_table",
			"cols": ["ID", "Process", "Name", "VMA", "Size", "Rss", "Pss", "Swap", "SwapPss", "Shared_Clean", "Shared_Dirty", "Private_Clean", "Private_Dirty", "Referenced", "Anonymous", "LazyFree", "KernelPageSize", "MMUPageSize", "AnonHugePages", "ShmemPmdMapped", "Shared_Hugetlb", "Private_Hugetlb", "Locked", "THPeligible", "FilePmdMapped"]
		}
	},
	"threads_dialog": {
		"title": "线程信息",
		"buttons": [{
			"id": "download_xml_threads", "name": "XML下载"
		}],
		"table": {
			"id": "threads_table",
			"cols": ["TID", "线程名", "PID", "进程名"]
		}
	},
	"symbol_calls_cnt": {
		"title": "符号调用信息",
		"buttons": [{
			"id": "download_xml_symbol_calls_cnt", "name": "XML下载"
		}],
		"table": {
			"id": "symbol_calls_cnt_table",
			"cols": ["符号ID", "模块ID", "符号名称", "demangle", "提供模块", "被调用次数"]
		}
	},
	"symbol_calls_details": {
		"title": "符号调用信息",
		"buttons": [{
			"id": "download_xml_symbol_calls_details", "name": "XML下载"
		}],
		"table": {
			"id": "symbol_calls_details_table",
			"cols": ["符号ID", "callee_id", "caller_id", "符号名称", "demangle", "提供模块", "调用模块"]
		}
	},
};

function _createDialogHeader(dialogId) {
	var content = '<div class="modal-header">\n';
	content += '<h5 class="modal-title">' + tableOptions[dialogId]["title"] + '</h5>\n';
	content += '<button type="button" class="close" data-dismiss="dialog" aria-label="Close" id="' + dialogId + '_close_btn"value="' + dialogId + '">\n';
	content += '<span aria-hidden="true">&times;</span>\n';
	content += '</button>\n';
	content += '</div>\n';
	return content;
}

function _createDialogButtons(dialogId) {
	if (!("buttons" in tableOptions[dialogId])) {
		return "";
	}
	var content = '<div class="btn-group me-2 float-end">\n';
	tableOptions[dialogId]["buttons"].forEach(function (btn, idx, arr) {
		content += '<button type="button" id="' + btn["id"] + '" class="btn btn-sm btn-outline-secondary">' + btn["name"] + '</button>\n';
	});
	content += '</div>\n';
	return content;
}

function _createDialogTables(dialogId) {
	if (!("table" in tableOptions[dialogId])) {
		return "";
	}

	var content = '<table cellpadding="0" cellspacing="0" border="0" class="display" id="' + tableOptions[dialogId]["table"]["id"] + '">\n';
	content += '<thead>\n<tr>\n';

	tableOptions[dialogId]["table"]["cols"].forEach(function (th, idx, arr) {
		content += '<th class="show_in_line">' + th + '</th>\n';
	});
	content += '</tr>\n</thead>\n';
	content += '<tbody>\n';
	content += '</tbody>\n';
	content += '</table>\n';

	return content;
}

function _createWrapperBlock(dialogId) {
	if (!("wrapper" in tableOptions[dialogId])) {
		return "";
	}

	return '<' + tableOptions[dialogId]["wrapper"]["type"] + ' id="' + tableOptions[dialogId]["wrapper"]["id"] + '">\n';
}

function _createDialogDiv(dialogId) {
	if (!("extra" in tableOptions[dialogId])) {
		return '<div class="modal-dialog modal-lg" role="document">\n';
	}
	var extra = tableOptions[dialogId]["extra"];
	var content = '<div class="modal-dialog modal-lg';

	if ("class" in extra) {
		content += " " + extra["class"];
	}

	content += '" role="document"';
	if ("style" in extra) {
		content += ' style="' + extra["style"] + '"';
	}

	content += '>\n';
	return content;
}

function createDialogWithTable(dialogId) {
	if (!(dialogId in tableOptions)) {
		return;
	}

	var content = _createDialogDiv(dialogId);
	content += '<div class="modal-content">\n';

	// Header
	content += _createDialogHeader(dialogId);

	// Body
	content += '<div class="modal-body">\n';

	//   wrapper item
	content += _createWrapperBlock(dialogId);

	//   buttons
	content += _createDialogButtons(dialogId);

	//   table
	content += _createDialogTables(dialogId);

	content += '</div>\n';

	// Footer
	content += '<div class="modal-footer">\n';
	content += '</div>\n';

	content += '</div>\n';
	content += '</div>\n';

	$("#"+dialogId).html(content);

	$("#"+dialogId+"_close_btn").click(function(evt) {
		var dialogId = $(evt.target).parent().val();
		$("#"+dialogId).modal("hide");
	});
}

function createProductMenus(currentPage) {
	$.get(REST_API_PREFIX + "products", function (data, status) {
		var products = JSON.parse(data)["data"];

		var curProduct = $.url().param('product');
		var curVersion = $.url().param('version');

		// Add dropdown menu button
		menuHtml = '<button id="current_product_btn" class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown">';
		if (!curProduct) {
			curProduct = products[1];
			curVersion = products[2];
		}
		menuHtml += curProduct + '/' + curVersion + '</button>\n';

		//$("#current_product_btn").text("test/test");
		var lastProduct = "";
		products.forEach(function (product, idx, arr) {
			if ((lastProduct === "") || (lastProduct != product[1])) {
				// End last product menu
				if (lastProduct === "") {
					menuHtml += '<ul class="dropdown-menu text-small">\n';
				} else {
					menuHtml += '</ul></li>\n';
				}
				// Add a new product menu
				menuHtml += '<li><a class="dropdown-item">' + product[1] + '&raquo;</a><ul class="dropdown-menu dropdown-submenu">\n';
				lastProduct = product[1];
			}

			// Add a product version submenu
			menuHtml += '<li><a class="dropdown-item" href="' + fnGetStaticHtmlUrl(currentPage, product[1], product[2]) + '">' + product[2] + '</a></li>\n';
		});
		// End last product
		menuHtml += '</ul></li></ul>\n';

		$("#products_menu").html(menuHtml);
	});
}

function  createSubPageMenus(currentPage, subpages) {
	var htmlContent = "";
	for (const [key, val] of Object.entries(subpages)) {
		var activeStr = "";
		if (currentPage === key) {
			activeStr = " btn-secondary";
		}
		//htmlContent += '<li class="nav-item' + activeStr +'" href="' + fnGetStaticHtmlUrl(key) + '">' + val + '</li>\n';
		htmlContent += '<li class="nav-item"><a class="btn btn-sm' + activeStr + '" href="' + fnGetStaticHtmlUrl(key) + '">' + val + '</a></li>\n';
	}
	$("#subpages_menu").html(htmlContent);
}

$(function createTopMenus() {
	var pages = {
		"modules": {"name": "模块列表"},
		"records": {"name": "内存信息"}
	};
	var currentPage = $.url().attr('file');
	if (currentPage === "") {
		currentPage = "modules.html"
	}
	currentPage = currentPage.substring(0, currentPage.indexOf(".html"));

	var htmlContent = "";
	for (const [key, val] of Object.entries(pages)) {
		var activeStr = "";
		if (currentPage === key) {
			activeStr = " active";
		} else if ("subpages" in val) {
			for (const [subpage, subname] of Object.entries(val["subpages"])) {
				if (currentPage === subpage) {
					activeStr = " active";
					break;
				}
			}
		}
		htmlContent += '<li class="nav-item"><a href="' + fnGetStaticHtmlUrl(key) + '" class="nav-link' + activeStr + '">' + val["name"] + '</a></li>\n';

		if ("subpages" in val && currentPage in val["subpages"]) {
			createSubPageMenus(currentPage, val["subpages"]);
		}
	};
	$("#pages_menu").html(htmlContent);

	createProductMenus(currentPage);

	$(".close").click(function(evt) {
		//console.log($(evt.target).parent());
		dialogId = $(evt.target).parent().val();
		$("#"+dialogId).modal("hide");
		//$(".modal").modal("hide");
	});
});

function fnArgsToURI(args) {
	var str = [];
	for(var p in args) {
		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(args[p]));
	}
	return str.join("&");
}

function fnGetStaticHtmlUrl(name, product, version, xargs) {
	if (xargs) {
		args = xargs;
	} else {
		args = {};
	}
	if (product && version) {
		args["product"] = product;
		args["version"] = version;
	} else {
		args["product"] = $.url().param('product');
		args["version"] = $.url().param('version');
	}
	return "/" + name + ".html?" + fnArgsToURI(args);
}

function fnGetAllItemsFilterUrl(name, xargs) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + name;

	if (xargs) {
		args = xargs;
	} else {
		args = {};
	}
	var params = $.url().param();
	for (const [key, val] of Object.entries(params)) {
		if (!["product", "version", "_"].includes(key)) {
			args[key] = val
		}
	};
	if (Object.keys(args).length > 0) {
		return url + "?" + fnArgsToURI(args);
	}
	return url;
}

function humanFileSize(size) {
        size = parseInt(size);
		var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
		return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

/*
 * Data Table Column Filter Related API
 */
(function($) {
/*
 * Function: fnGetColumnData
 * Purpose:  Return an array of table values from a particular column.
 * Returns:  array string: 1d data array 
 * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function
 *				   int:iColumn - the id of the column to extract the data from
 *				   bool:bUnique - optional - if set to false duplicated values are not filtered out
 *				   bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)
 *				   bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array
 */
$.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
	// check that we have a column id
	if ( typeof iColumn == "undefined" ) return new Array();
	
	// by default we only wany unique data
	if ( typeof bUnique == "undefined" ) bUnique = true;
	
	// by default we do want to only look at filtered data
	if ( typeof bFiltered == "undefined" ) bFiltered = true;
	
	// by default we do not wany to include empty values
	if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
	
	// list of rows which we're going to loop through
	var aiRows;
	
	// use only filtered rows
	if (bFiltered == true) aiRows = oSettings.aiDisplay; 
	// use all rows
	else aiRows = oSettings.aiDisplayMaster; // all row numbers

	// set up data array	
	var asResultData = new Array();
	
	for (var i=0,c=aiRows.length; i<c; i++) {
		iRow = aiRows[i];
		var aData = this.fnGetData(iRow);
		var sValue = aData[iColumn];
		
		// ignore empty values?
		if (bIgnoreEmpty == true && sValue.length == 0) continue;

		// ignore unique values?
		else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;
		
		// else push the value onto the result data array
		else asResultData.push(sValue);
	}
	
	return asResultData;
}}(jQuery));

function fnCreateSelect( aData ) {
	var r='<select><option value=""></option>', i;
	var iLen = aData.length;
	for ( i=0 ; i<iLen ; i++ ) {
		r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
	}
	return r+'</select>';
}

/*
 * Modules related API
 */

function fnModuleCommonRowCallback(aData, nRow) {
	/* Human readble size */
	$('td:eq(23)', nRow).text( humanFileSize(aData[25]) );
}

function fnModuleTableCommonProc(moduleTable, selectProc) {
	$("#modules tbody").on("click", "tr", function() {
		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
		} else {
			moduleTable.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}

		if (selectProc) {
			selectProc(moduleTable.fnGetData(this));
		}
	});
}

function fnGetModulesRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "modules/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnGetDepsRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "deps/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnEvtShowCallTables(id, type) {
	createDialogWithTable("symbol_dialog");

	var url = fnGetModulesRestUrl(id, "fields", {"type":type});
	var symbolTable = $('#symbol_table').dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollY": "300px"
	} );
	$('#download_xml_symbols').unbind ('click').click(id, function (evt) {
		window.open(fnGetModulesRestUrl(evt.data, "fields", {"type":type, "format":"xml"}));
	});
	$('#symbol_dialog').appendTo("body").modal("show");
}

function fnEvtShowSymbolsForDeps(id) {
	createDialogWithTable("callinfo_dialog");

	var url = fnGetDepsRestUrl(id, "symbols");
	var symbolTable = $('#callinfo_table').dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true
	} );
	$('#download_xml_callinfo').unbind ('click').click(id, function (evt) {
		window.open(fnGetDepsRestUrl(evt.data, "symbols", {"format":"xml"}));
	});
	$('#callinfo_dialog').appendTo("body").modal("show");
}

function fnEvtShowDepsTables(id, type) {
	createDialogWithTable("deps_dialog");

	url = fnGetModulesRestUrl(id, "fields", {"type": type});
	var symbolTable = $('#depends').dataTable( {
		"bProcessing": true,
		"pageLength": 15,
		"bPaginate": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"columnDefs": [{
			visible: false,
			targets: [0,1,2],
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			/* Append the grade to the default row class name */
			dataToLink = aData[5];
			if (dataToLink > 0) {
				$('td:eq(2)', nRow).html( '<a href="javascript:fnEvtShowSymbolsForDeps(' + aData[0] + ')">' + dataToLink + '</a>' );
			}

			var link = '<a href="javascript:fnShowModuleDetails(\'' + aData[1] + '\')">' + aData[3] + '</a>';
			$('td:eq(0)', nRow).html(link);

			var link = '<a href="javascript:fnShowModuleDetails(\'' + aData[2] + '\')">' + aData[4] + '</a>';
			$('td:eq(1)', nRow).html(link);
			return nRow;
		}
	} );
	$('#download_xml_deps').unbind ('click').click(id, function (evt) {
		window.open(fnGetModulesRestUrl(evt.data, "fields", {"type":type, "format":"xml", "details":1}));
	});
	$('#download_svg_deps').unbind ('click').click(id, function (evt) {
		window.open(fnGetModulesRestUrl(evt.data, "fields", {"type":type, "format":"svg"}));
	});
	$('#download_png_deps').unbind ('click').click(id, function (evt) {
		window.open(fnGetModulesRestUrl(evt.data, "fields", {"type":type, "format":"png"}));
	});
	$('#download_dot_deps').unbind ('click').click(id, function (evt) {
		window.open(fnGetModulesRestUrl(evt.data, "fields", {"type":type, "format":"dot"}));
	});
	$('#deps_dialog').appendTo("body").modal("show");

	setTimeout(function() {
		$("#depends").attr("style", "width:100%");
	}, 10);
}

function fnShowHelpInfo(type) {
	createDialogWithTable("help_info_dialog");
	$("#help_info_img").attr("src", "/images/" + type);
	$('#help_info_dialog').appendTo("body").modal("show");
}

function fnEvtFuncCallGraph (rowData, category) {
	var module_id = rowData[0];

	xargs = {"type":"deps", "format":"svg"};
	if (category) {
		xargs["__category"] = category;
	}
	$("#svg_callgraph").attr("data", fnGetModulesRestUrl(module_id, "fields", xargs));
}

var total_colums = 42;
var hidden_cols = 2;

var select_types = ["content", "range", "list", "dict"];
var filterDict = {
	"module_footer": {"idx": hidden_cols, "type": "edit"},
	"deps_footer": {"idx": hidden_cols + 1, "type": "range", "range": [{"val":0}, {"min":0}, {"min":0, "max":10}, {"min":10}]},
	"dependedBy_footer": {"idx": hidden_cols + 2, "type": "range", "range": [{"val":0}, {"min":0}, {"min":0, "max":10}, {"min":10}]},
};

function matchWithRange(val, range) {
	//console.log("match " + val + " with range: " + getRangeName(range));
	if ("val" in range) {
		if (val == range["val"]) {
			return true;
		}
		return false;
	} else if ("min" in range) {
		if ("max" in range) {
			if ((val > range["min"]) && (val < range["max"])) {
				return true;
			}
			return false;
		}
		if (val > range["min"]) {
			return true;
		}
		return false;
	} else if ("max" in range) {
		if (val < range["max"]) {
			return true;
		}
		return false;
	}
	return true;
}

function dataMatchWithFilterDict(data, ele, filter) {
	if (select_types.includes(filter["type"])) {
		ele = ele + " select";
	} else {
		ele = ele + " input";
	}
	var filterVal = $(ele).val();
	if (filterVal === undefined || (filterVal === "")) {
		//console.log(ele + ": [" + filterVal + "] no need filter");
		return true;
	}
	if (filter["type"] == "range") {
		if (filterVal == 0 || filterVal === "0") {
			return true;
		}
		return matchWithRange(data[filter["idx"]], filter["range"][filterVal - 1]);
	}
	if (select_types.includes(filter["type"])) {
		if (data[filter["idx"]] === filterVal) {
			return true;
		}
		return false;
	}
	if (!data[filter["idx"]].includes(filterVal)) {
		//console.log("filter val [" + filterVal + " not matched.");
		return false;
	}
	return true;
}

function getRangeName(range) {
	if ("val" in range) {
		return range["val"];
	} else if ("min" in range) {
		if ("max" in range) {
			return range["min"] + "-" + range["max"];
		}
		return ">" + range["min"];
	} else if ("max" in range) {
		return "<" + range["max"];
	}
	return "";
}

function fnCreateRangeSelect(filter) {
	var r='<select><option value=></option>';
	filter["range"].forEach(function (val, idx, arr) {
		idx = idx + 1;
		r += '<option value=' + idx + '>' + getRangeName(val) + '</option>';
	});
	return r+'</select>';
}

function fnCreateListSelect(filter) {
	var r='<select><option value=></option>';
	filter["list"].forEach(function (val, idx, arr) {
		r += '<option value=' + val + '>' + val + '</option>';
	});
	return r+'</select>';
}

function fnCreateDictSelect(filter) {
	var r='<select><option value=></option>';
	for (val in filter["dict"]) {
		r += '<option value="' + val + '">' + filter["dict"][val] + '</option>';
	};
	return r+'</select>';
}

function createStaticFilterFooters(moduleTable) {
	$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
		if (settings.sTableId != "modules") {
			return true;
		}
		for (var ele_id in filterDict) {
			var ele = '#' + ele_id;
			filter = filterDict[ele_id];
			if (!dataMatchWithFilterDict(data, ele, filter)) {
				return false;
			}
		}
		return true;
	});

	for (var ele_id in filterDict) {
		var ele = '#' + ele_id;
		filter = filterDict[ele_id];
		if (filter["type"] == "edit") {
			$(ele).html('<input type="text">');
			$("input", ele).keyup( function () {
				moduleTable.fnDraw();
			} );
			continue;
		}
		if (filter["type"] == "range") {
			$(ele).html(fnCreateRangeSelect(filter));
			$("select", ele).change( function () {
				moduleTable.fnDraw();
			} );
			continue;
		}
		if (filter["type"] == "list") {
			$(ele).html(fnCreateListSelect(filter));
			$("select", ele).change( function () {
				moduleTable.fnDraw();
			} );
			continue;
		}
		if (filter["type"] == "dict") {
			$(ele).html(fnCreateDictSelect(filter));
			$("select", ele).change( function () {
				moduleTable.fnDraw();
			} );
			continue;
		}
	};
}

function createDynamicFilterFooters(moduleTable) {
	window.setTimeout( function () {
		for (var ele_id in filterDict) {
			var ele = '#' + ele_id;
			filter = filterDict[ele_id];
			if (filter["type"] != "content") {
				continue;
			}
			$(ele).html(fnCreateSelect( moduleTable.fnGetColumnData(filter["idx"]) ));
			$('select', $(ele)).change( function () {
				moduleTable.fnDraw();
			} );
		}
	}, 1500);
}

var details_dict = {
	"basic_info": {
		"name": "基础信息",
		"rows": [{
			"key": "category",
			"name": "类型"
		}, {
			"key": "path",
			"name": "模块路径"
		}]
	},
	"resource_info": {
		"name": "ROM&RAM消耗",
		"rows": [{
			"key": "size",
			"name": ["ROM", "文件大小"]
		}, {
			"key": "text_size",
			"name": ["ROM", "text段大小"]
		}, {
			"key": "data_size",
			"name": ["ROM", "data段大小"]
		}, {
			"key": "oc_size",
			"name": ["ROM", "Object C段大小"]
		}, {
			"key": "processes",
			"name": ["RAM", "被加载的进程数"]
		}, {
			"key": "Pss",
			"name": ["RAM", "Pss内存信息"]
		}, {
			"key": "swapPss",
			"name": ["RAM", "Pss内存信息"]
		}, {
			"key": "Shared_Dirty",
			"name": ["RAM", "Shared_Dirty内存信息"]
		}, {
			"key": "Private_Dirty",
			"name": ["RAM", "Pss内存信息"]
		}]
	},
	"deps_info": {
		"name": "依赖信息",
		"rows": [{
			"name": ['<a href="javascript:fnShowHelpInfo(\'deps.png\')">详细定义</a>', "英文", "含义", "数值"]
		}, {
			"key": "deps",
			"name": ["D(n)", "deps", "直接依赖模块个数"]
		}, {
			"key": "deps_indirect",
			"name": ["D_I(n)", "deps_indirect", "间接依赖模块个数"]
		}, {
			"key": "deps_total",
			"name": ["D_T(n)", "deps_total", "依赖模块总个数"]
		}, {
			"key": "depth",
			"name": ["D_D", "deps_depth", "依赖深度"]
		}]
	},
	"dependedBy_info": {
		"name": "被依赖信息",
		"rows": [{
			"name": ['<a href="javascript:fnShowHelpInfo(\'dependedBy.png\')">详细定义</a>', "英文", "含义", "数值"]
		}, {
			"key": "dependedBy",
			"name": ["B(n)", "dependedBy", "被直接依赖模块个数"]
		}, {
			"key": "dependedBy_indirect",
			"name": ["B_I(n)", "dependedBy_indirect", "被间接依赖模块个数"]
		}, {
			"key": "dependedBy_total",
			"name": ["B_T(n)", "dependedBy_total", "被依赖模块总个数"]
		}, {
			"key": "dependedBy_depth",
			"name": ["B_D", "dependedBy_depth", "被依赖深度"]
		},]
	},
	"symbols_info": {
		"name": "符号信息",
		"rows": [{
			"name": ['<a href="javascript:fnShowHelpInfo(\'symbols.png\')">详细定义</a>', "英文", "含义", "数值"]
		}, {
			"key": "provided",
			"name": ["S_P(n)", "provided", "提供的符号个数"]
		}, {
			"key": "public_symbols",
			"name": ["S_P(n)", "public_symbols", "公共符号个数"]
		}, {
			"key": "used",
			"name": ["S_Pt(n)", "truely provided", "被使用到的符号个数"]
		}, {
			"key": "unused",
			"name": ["S_Pf(n)", "falsely provided", "未被使用到的符号个数"]
		}, {
			"key": "needed",
			"name": ["S_U(n)", "undefined", "未定义符号个数"]
		}, {
			"key": "matched",
			"name": ["S_Ut(n)", "matched undefined", "与依赖库匹配的未定义符号个数"]
		}, {
			"key": "unmatched",
			"name": ["S_Pf(n)", "unmatched undefined", "与依赖库无法匹配的未定义符号个数"]
		}, {
			"key": "duplicated",
			"name": ["S_Ud(n)", "duplicates", "与依赖库匹配的重复未定义符号个数"]
		}]
	}
};

function __generateDetailValue(category, key, moduleId, moduleData) {
	if (key === "dependedBy_depth") { // No link
		return moduleData[key];
	}
	if (key === "processes") {
		if (moduleData[key] > 0) {
			return '<a href="javascript:fnEvtShowProcessTables(' + moduleId + ')">' + humanFileSize(moduleData[key]) + '</a>';
		}
	} else if ((key === "depth") && (moduleData[key] > 0)) {
		return '<a href="' + fnGetModulesRestUrl(moduleId, "fields", {"type":key, "format":"png"}) +'" target="_blank">' + moduleData[key] + '<a/>';
	} else if ((category === "deps_info") || (category === "dependedBy_info")) {
		if (moduleData[key] > 0) {
			return  '<a href="javascript:fnEvtShowDepsTables(' + moduleId + ', \'' + key + '\')">' + moduleData[key] + '</a>';
		}
	} else if (category === "symbols_info") {
		if (moduleData[key] > 0) {
			return  '<a href="javascript:fnEvtShowCallTables(' + moduleId + ', \'' + key + '\')">' + moduleData[key] + '</a>';
		}
	} else if (category === "resource_info") {
		return humanFileSize(moduleData[key]);
	} else if (category === "basic_info") {
		if (key === "sa_id") {
			return moduleData[key];
		}
		if (key === "chipset") {
			if (moduleData[key] === "0") {
				return "系统组件";
			}
			return "芯片组件";
		}
		if (moduleData[key] === "0") {
			return "否";
		} else if (moduleData[key] === "1") {
			return "是";
		}
	}
	return moduleData[key];
}

function _generateCollapse(parent, name, info, show, moduleId, moduleData) {
	var header = '<div class="card">';

	// Header div
	header += '<div class="card-header" id="heading' + name + '">';
	header += '<h5 class="mb-0">';
	header += '<button class="btn btn-link" id="collapse_btn_' + name + '">';
	header += info["name"] + '</button>';
	header += '</h5>';
	header += '</div>';

	// collapse content div
	header += '<div id="collapse_' + name + '" class="collapse ' + show + '" aria-labelledby="heading' + name + '" data-parent="#' + parent + '">';
	header += '<div class="container">';

	info["rows"].forEach(function (val, idx, arr) {
		header += '<div class="row border-bottom">';
		if (Array.isArray(val["name"])) {
			val["name"].forEach(function (subName, subIdx, subArr) {
				header += '<div class="col border-end">' + subName + '</div>';
			});
		} else {
			header += '<div class="col border-end">' + val["name"] + '</div>';
		}
		if ("key" in val) {
			header += '<div class="col text-end">' + __generateDetailValue(name, val["key"], moduleId, moduleData) + '</div>';
		}
		header += '</div>';
	});

	header += '</div>';
	header += '</div>';

	header += '</div>';
	return header;
}

function _addCollapseClickFunc() {
	for (var collaps in details_dict) {
		$("#collapse_btn_"+collaps).unbind ('click').click(collaps, function (evt) {
			$("#collapse_"+evt.data).toggle();
		});
	}
}

function fnShowModuleDetails(moduleId) {
	$.get(fnGetModulesRestUrl(moduleId, "details", {"format":"json"}), function (data, status) {
		createDialogWithTable("module_dialog");
		var detailHtml = "";
		var show = "show";
		for (var collaps in details_dict) {
			detailHtml += _generateCollapse("accordion", collaps, details_dict[collaps], show, moduleId, JSON.parse(data));
			show = "hide";
		}
		$("#accordion").html(detailHtml);
		$('#module_dialog').appendTo("body").modal("show");
		_addCollapseClickFunc();
	});
}

var moduleTable;

function loadModules() {
	let symbol_table = new DataTable('#symbol_table', {
	});

	$('#all_modules').click(function () {
		window.open(fnGetAllItemsFilterUrl("modules", {"format":"xml"}));
	});

	moduleTable = $('#modules').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetAllItemsFilterUrl("modules"),
		"sScrollX": "1000px",
		//"sScrollY": "400px",
		"bPaginate": true,
		"columnDefs": [{
			visible: false,
			targets: [0,1],
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			fnModuleCommonRowCallback(aData, nRow);

			pos = 2;
			var link = '<a href="javascript:fnShowModuleDetails(' + aData[0] + ")\">" + aData[pos] + '</a>';
			var ele = 'td:eq(' + (pos-2).toString() + ')';
			$(ele, nRow).html(link);

			var startIdx = 3;
			["deps", "dependedBy"].forEach(function (val, idx, arr) {
				pos = startIdx+idx;
				if (aData[pos] > 0) {
					var link = '<a href="javascript:fnEvtShowDepsTables(' + aData[0] + ", '" + val + "')\">" + aData[pos] + '</a>';
					var ele = 'td:eq(' + (pos-2).toString() + ')';
					$(ele, nRow).html(link);
				}
			});

			startIdx = 5;
			["provided", "public_symbols", "used", "unused", "needed", "matched", "duplicated", "unmatched"].forEach(function (val, idx, arr) {
				pos = startIdx+idx;
				if (aData[pos] > 0) {
					var link = '<a href="javascript:fnEvtShowCallTables(' + aData[0] + ", '" + val + "')\">" + aData[pos] + '</a>';
					var ele = 'td:eq(' + (pos-2).toString() + ')';
					$(ele, nRow).html(link);
				}
			});

			pos = 18;
			if (aData[pos] > 0) {
				var link = '<a href="javascript:fnEvtShowProcessTables(' + aData[1] + ")\">" + aData[pos] + '</a>';
				var ele = 'td:eq(' + (pos-2).toString() + ')';
				$(ele, nRow).html(link);
			}

			/* Human readble size */
			//$('td:eq(10)', nRow).text( humanFileSize(aData[26]) );
			//$('td:eq(11)', nRow).text( humanFileSize(aData[27]) );

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable, function (selectedData) {
		fnEvtFuncCallGraph(selectedData);
	});
	createDynamicFilterFooters(moduleTable);
	createStaticFilterFooters(moduleTable);
}

function fnGetProcessesRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "processes/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnEvtShowProcessObjectsTables(id) {
	createDialogWithTable("objects_dialog");

	url = fnGetProcessesRestUrl(id, "objects", {});
	var objectsTable = $("#objects_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollX": "1000px",
		"sScrollY": "300px",
		"columnDefs": [{
			visible: false,
			targets: [0],
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowProcessVmasTables(' + aData[0] + ',\'object\')">' + aData[3] + '</a>';
			$('td:eq(2)', nRow).html(link);
			return nRow;
		}
	} );
	$('#download_xml_objects').unbind ('click').click(id, function (evt) {
		window.open(fnGetProcessesRestUrl(evt.data, "objects", {"format":"xml"}));
	});
	$("#objects_dialog").appendTo("body").modal("show");
}

function fnEvtShowProcessThreadsTables(id) {
	createDialogWithTable("threads_dialog");
	url = fnGetProcessesRestUrl(id, "threads", {});
	var objectsTable = $("#threads_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollY": "300px"
	} );
	$('#download_xml_threads').unbind ('click').click(id, function (evt) {
		window.open(fnGetProcessesRestUrl(evt.data, "threads", {"format":"xml"}));
	});
	$("#threads_dialog").appendTo("body").modal("show");
}

function fnEvtShowProcessVmasTables(id, type) {
	createDialogWithTable("vmas_dialog");
	url = fnGetProcessesRestUrl(id, "vmas", {"type": type});
	var symbolTable = $("#vmas_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollX": "1000px",
		"sScrollY": "300px",
		"columnDefs": [{
			visible: false,
			target: 0,
		}]
	} );
	$('#download_xml_vmas').unbind ('click').click(id, function (evt) {
		window.open(fnGetProcessesRestUrl(evt.data, "vmas", {"type": type, "format":"xml"}));
	});
	$("#vmas_dialog").appendTo("body").modal("show");
}

function loadProcesses() {
	$('#all_modules').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("processes", {"format":"xml"}));
	});
	$('#all_module_details').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("processes", {"format":"xml", "details": 1}));
	});

	var moduleTable = $('#modules').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetAllItemsFilterUrl("processes", {}),
		"pageLength": 15,
		"bPaginate": true,
		"columnDefs": [{
			visible: false,
			targets: [ 0, 1 ],
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowProcessThreadsTables(' + aData[0] + ', \'process\')">' + aData[5] + '</a>';
			$('td:eq(3)', nRow).html(link);

			var link = '<a href="javascript:fnEvtShowProcessObjectsTables(' + aData[0] + ')">' + aData[6] + '</a>';
			$('td:eq(4)', nRow).html(link);

			var link = '<a href="javascript:fnEvtShowProcessVmasTables(' + aData[0] + ', \'process\')">' + aData[7] + '</a>';
			$('td:eq(5)', nRow).html(link);

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable);
}

function fnGetObjectsRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "objects/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnEvtShowProcessTables(id) {
	createDialogWithTable("processes_dialog");

	url = fnGetObjectsRestUrl(id, "processes", {});
	var objectsTable = $("#processes_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollX": "1000px",
		"sScrollY": "300px",
		"columnDefs": [{
			visible: false,
			targets: [0, 1]
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowProcessVmasTables(' + aData[0] + ',\'object\')">' + aData[4] + '</a>';
			$('td:eq(2)', nRow).html(link);
			return nRow;
		}
	} );
	$('#download_xml_processes').unbind ('click').click(id, function (evt) {
		window.open(fnGetObjectsRestUrl(evt.data, "processes", {"format":"xml"}));
	});
	$("#processes_dialog").appendTo("body").modal("show");
}

function loadProcessObjects() {
	$('#all_modules').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("objects", {"format":"xml"}));
	});

	var moduleTable = $('#modules').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetAllItemsFilterUrl("objects", {}),
		"pageLength": 15,
		"bPaginate": true,
		"columnDefs": [{
			visible: false,
			target: [0, 1],
		}],
		"order": [[2, 'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowProcessTables(' + aData[0] + ')">' + aData[3] + '</a>';
			$('td:eq(1)', nRow).html(link);

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable);
}

function fnGetThreadsRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "threads/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnEvtShowThreadsByName(id) {
	createDialogWithTable("threads_dialog");
	url = fnGetThreadsRestUrl(id, "threads", {});
	var objectsTable = $("#threads_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollY": "300px",
		"order": [[3, 'desc']],
	} );
	$('#download_xml_threads').unbind ('click').click(id, function (evt) {
		window.open(fnGetThreadsRestUrl(evt.data, "threads", {"format":"xml"}));
	});
	$("#threads_dialog").appendTo("body").modal("show");
}

function loadNamedThreads() {
	$('#all_modules').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("objects", {"format":"xml"}));
	});
	$('#all_module_details').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("objects", {"format":"xml", "details":1}));
	});

	// fnEvtShowProcessThreadsTables
	var moduleTable = $('#modules').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetAllItemsFilterUrl("threads", {}),
		"pageLength": 15,
		"bPaginate": false,
		"order": [[1, 'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowThreadsByName(\'' + aData[0] + '\')">' + aData[1] + '</a>';
			$('td:eq(1)', nRow).html(link);

			var link = '<a href="javascript:fnEvtShowThreadsByName(\'' + aData[0] + '\')">' + aData[2] + '</a>';
			$('td:eq(2)', nRow).html(link);

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable);
}

function fnGetSymbolsRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "symbols/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnEvtShowCallDetailsBySymbolId(id) {
	createDialogWithTable("symbol_calls_details");

	var url = fnGetSymbolsRestUrl(id, "details");
	var symbolTable = $('#symbol_calls_details_table').dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"columnDefs": [{
			visible: false,
			targets: [0,1,2,3,4],
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnShowModuleDetails(\'' + aData[1] + '\')">' + aData[5] + '</a>';
			$('td:eq(0)', nRow).html(link);

			link = '<a href="javascript:fnShowModuleDetails(\'' + aData[2] + '\')">' + aData[6] + '</a>';
			$('td:eq(1)', nRow).html(link);

			return nRow;
		}
	} );
	$('#download_xml_symbol_calls_details').unbind ('click').click(id, function (evt) {
		window.open(fnGetSymbolsRestUrl(evt.data, "details", {"format":"xml"}));
	});
	$('#symbol_calls_details').appendTo("body").modal("show");
}

function fnEvtShowCallsCntBySymbolName(id, type) {
	createDialogWithTable("symbol_calls_cnt");

	var url = fnGetSymbolsRestUrl(id, type);
	var symbolTable = $('#symbol_calls_cnt_table').dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"columnDefs": [{
			visible: false,
			targets: [0,1,2,3],
		}],
		"order": [[5, 'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnShowModuleDetails(\'' + aData[1] + '\')">' + aData[4] + '</a>';
			$('td:eq(0)', nRow).html(link);

			if (aData[5] > 0) {
				var link = '<a href="javascript:fnEvtShowCallDetailsBySymbolId(\'' + aData[0] + '\')">' + aData[5] + '</a>';
				$('td:eq(1)', nRow).html(link);
			}

			return nRow;
		}
	} );
	$('#download_xml_symbol_calls_cnt').unbind ('click').click(id, function (evt) {
		window.open(fnGetSymbolsRestUrl(evt.data, type, {"format":"xml"}));
	});
	$('#symbol_calls_cnt').appendTo("body").modal("show");
}

function loadDuplicatedSymbols() {
	$('#all_modules').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("symbols", {"format":"xml"}));
	});
	$('#all_module_details').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("symbols", {"format":"xml", "details":1}));
	});

	var moduleTable = $('#dup_symbols').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetAllItemsFilterUrl("symbols", {}),
		"pageLength": 15,
		"bPaginate": true,
		"columnDefs": [{
			visible: false,
			targets: [0],
		}],
		"order": [[1, 'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (aData[1] > 0) {
				var link = '<a href="javascript:fnEvtShowCallsCntBySymbolName(\'' + aData[0] + '\', \'modules\')">' + aData[1] + '</a>';
				$('td:eq(0)', nRow).html(link);
			}

			if (aData[2] > 0) {
				var link = '<a href="javascript:fnEvtShowCallsCntBySymbolName(\'' + aData[0] + '\', \'calls\')">' + aData[2] + '</a>';
				$('td:eq(1)', nRow).html(link);
			}

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable);
}

function  createSearchedSymbolsTable() {
	$("#search_symbols_div").html('<div class="btn-group me-2 float-end">\n' +
				'<button type="button" id="all_modules" class="btn btn-sm btn-outline-secondary">XML下载</button>\n' +
			'</div>\n' +
			'<table cellpadding="0" cellspacing="0" border="0" class="display" id="search_symbols">\n' +
			'<thead>\n' +
				'<tr>\n' +
					'<th>原名</th>\n' +
					'<th style="text-align: center;" class="show_in_line table_header_second">定义次数</th>\n' +
					'<th style="text-align: center;" class="show_in_line table_header_second">被调用次数</th>\n' +
					'<th style="text-align: center;" class="show_in_line table_header_second">符号名称</th>\n' +
				'</tr>\n' +
			'</thead>\n' +
			'<tbody>\n' +
			'</tbody>\n' +
			'</table>\n');
}

function searchSymbol() {
	createSearchedSymbolsTable();

	var searchStr = $("#search_symbol_text").val();
	var url = fnGetSymbolsRestUrl(searchStr, "search", {"format": "xml"});
	$('#all_modules').unbind ('click').click(function () {
		window.open(url);
	});

	var moduleTable = $('#search_symbols').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetSymbolsRestUrl(searchStr, "search"),
		"pageLength": 15,
		"bPaginate": true,
		"columnDefs": [{
			visible: false,
			targets: [0],
		}],
		"order": [[1, 'desc']],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (aData[1] > 0) {
				//var link = '<a href="javascript:fnEvtShowCallsCntBySymbolName(\'' + aData[0] + '\', \'modules\')">' + aData[1] + '</a>';
				//$('td:eq(0)', nRow).html(link);
			}

			if (aData[2] > 0) {
				var link = '<a href="javascript:fnEvtShowCallsCntBySymbolName(\'' + aData[0] + '\', \'calls\')">' + aData[2] + '</a>';
				$('td:eq(1)', nRow).html(link);
			}

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable);
}

function fnGetMemRecordsRestUrl(id, cmd, args) {
	prefix = fnGetProductRestPrefix();
	url =  prefix + "records/" + id + "/" + cmd;

	if (args) {
		url = url + "?" + fnArgsToURI(args);
	}
	return url;
}

function fnEvtShowProcessesDetails(id, type, region_type) {
	createDialogWithTable("processes_by_name_dialog");

    args = {}
    if (type === "processes_by_type_name") {
        args["region_type"] = region_type;
    }

	url = fnGetMemRecordsRestUrl(id, type, args);
	var objectsTable = $("#processes_by_name_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollX": "1000px",
		"sScrollY": "300px",
		"columnDefs": [{
			visible: false,
			targets: [0, 1]
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		    var link = '<a href="javascript:fnEvtShowRegionTypesTables(\'' + aData[0] + '\',\'regiontypes_by_process\')">' + aData[9] + '</a>';
			$('td:eq(7)', nRow).html(link);

            var link = '<a href="javascript:fnEvtShowRegionDetailsTables(\'' + aData[0] + '\',\'regions_by_process\')">' + aData[10] + '</a>';
			$('td:eq(8)', nRow).html(link);
			return nRow;
		}
	} );
	$('#download_xml_processes').unbind ('click').click(id, function (evt) {
	    args["format"] = "xml";
		window.open(fnGetMemRecordsRestUrl(evt.data, type, args));
	});
	$("#processes_by_name_dialog").appendTo("body").modal("show");
}

function fnEvtShowProcessesTables(id) {
	createDialogWithTable("processes_dialog");

	url = fnGetMemRecordsRestUrl(id, "processes", {});
	var objectsTable = $("#processes_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollX": "1000px",
		"sScrollY": "300px",
		"columnDefs": [{
			visible: false,
			targets: [0, 1, 2]
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowProcessesDetails(' + aData[0] + ',\'processes_by_name\')">' + aData[5] + '</a>';
			$('td:eq(2)', nRow).html(link);

			var link = '<a href="javascript:fnEvtShowRegionTypesTables(\'' + aData[0] + '\',\'regiontypes_by_process\')">' + aData[9] + '</a>';
			$('td:eq(6)', nRow).html(link);

            var link = '<a href="javascript:fnEvtShowRegionDetailsTables(\'' + aData[0] + '\',\'regions_by_process\')">' + aData[10] + '</a>';
			$('td:eq(7)', nRow).html(link);
			return nRow;
		}
	} );
	$('#download_xml_processes').unbind ('click').click(id, function (evt) {
		window.open(fnGetMemRecordsRestUrl(evt.data, "processes", {"format":"xml"}));
	});
	$("#processes_dialog").appendTo("body").modal("show");
}

function fnEvtShowRegionDetailsTables(id, type, name) {
	createDialogWithTable("region_details_dialog");

    args = {}
    if (name) {
        args["name"] = name;
    }

	url = fnGetMemRecordsRestUrl(id, type, args);
	var objectsTable = $("#region_details_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"sScrollX": "1000px",
		"sScrollY": "300px",
		"columnDefs": [{
			visible: false,
			targets: []
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			// var link = '<a href="javascript:fnEvtShowProcessVmasTables(' + aData[0] + ',\'object\')">' + aData[4] + '</a>';
			// $('td:eq(2)', nRow).html(link);
			return nRow;
		}
	} );
	$('#download_xml_region_details').unbind ('click').click(id, function (evt) {
	    args["format"] = "xml";
		window.open(fnGetMemRecordsRestUrl(evt.data, type, args));
	});
	$("#region_details_dialog").appendTo("body").modal("show");
}

function fnEvtShowRegionTypesTables(id, type) {
	createDialogWithTable("region_types_dialog");

	url = fnGetMemRecordsRestUrl(id, type, {});
	hidden = [0, 1];
	hiddenCnt = 2;
	if (type === "regiontypes_by_process") {
	    hidden = [0, 1, 2, 3];
	    hiddenCnt = 4;
	}
	var objectsTable = $("#region_types_table").dataTable( {
		"bProcessing": true,
		"sAjaxSource": url,
		"bDestroy": true,
		"columnDefs": [{
			visible: false,
			targets: hidden
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		    if (type === "regiontypes") {
                var link = '<a href="javascript:fnEvtShowProcessesDetails(' + id + ',\'processes_by_type_name\',\'' + aData[4] + '\')">' + aData[2] + '</a>';
                $('td:eq(0)', nRow).html(link);

                var link = '<a href="javascript:fnEvtShowRegionTypesTables(\'' + aData[4] + '\',\'regiontypes_by_name\')">' + aData[4] + '</a>';
                $('td:eq(2)', nRow).html(link);

                var link = '<a href="javascript:fnEvtShowRegionDetailsTables(' + id + ',\'regions_by_type_name\',\'' + aData[4] + '\')">' + aData[5] + '</a>';
                $('td:eq(3)', nRow).html(link);
			} else if (type === "regiontypes_by_name") {
			    var link = '<a href="javascript:fnEvtShowProcessesDetails(' + aData[1] + ',\'processes_by_type_name\',\'' + aData[4] + '\')">' + aData[2] + '</a>';
                $('td:eq(0)', nRow).html(link);

                var link = '<a href="javascript:fnEvtShowRegionDetailsTables(' + aData[1] + ',\'regions_by_type_name\',\'' + aData[4] + '\')">' + aData[5] + '</a>';
                $('td:eq(3)', nRow).html(link);
			}
			else {
                var link = '<a href="javascript:fnEvtShowRegionDetailsTables(\'' + aData[0] + '\',\'regions_by_type\')">' + aData[5] + '</a>';
                $('td:eq(1)', nRow).html(link);
			}

			return nRow;
		}
	} );
	$('#download_xml_region_types').unbind ('click').click(id, function (evt) {
		window.open(fnGetMemRecordsRestUrl(evt.data, type, {"format":"xml"}));
	});
	$("#region_types_dialog").appendTo("body").modal("show");
}

function loadMemRecords() {
	$('#all_records').unbind ('click').click(function () {
		window.open(fnGetAllItemsFilterUrl("records", {"format":"xml"}));
	});

	var moduleTable = $('#records').dataTable( {
		"bProcessing": true,
		"sAjaxSource": fnGetAllItemsFilterUrl("records", {}),
		"pageLength": 15,
		"bPaginate": true,
		"columnDefs": [{
			visible: false,
			targets: [],
		}],
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			var link = '<a href="javascript:fnEvtShowProcessesTables(' + aData[0] + ', \'process\')">' + aData[2] + '</a>';
			$('td:eq(2)', nRow).html(link);

			var link = '<a href="javascript:fnEvtShowRegionTypesTables(' + aData[0] + ', \'regiontypes\')">' + aData[4] + '</a>';
			$('td:eq(4)', nRow).html(link);

			return nRow;
		}
	} );

	fnModuleTableCommonProc(moduleTable);
}
