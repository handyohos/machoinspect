#!/usr/bin/env python
#coding=utf-8

import config

if __name__ == '__main__':
    import sys
    if sys.version_info > (3, 0):
        from http.server import HTTPServer
    else:
        from BaseHTTPServer import HTTPServer
    import rest

    try:
        myServer = HTTPServer((config.SERVER_IP, config.SERVER_PORT), rest.OhosArchInfoRequestHandler)
        print('Symbol Database Server started on %s:%d.' % (config.SERVER_IP, config.SERVER_PORT))
        myServer.serve_forever()
    except:
        print('Symbol Database Server failed on %s:%d!' % (config.SERVER_IP, config.SERVER_PORT))

    sys.exit(0)
