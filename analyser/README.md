analyser模块用于对[collector](../collector/README.md)收集到的结构化架构信息数据进行分析并展示。

其中REST服务提供数据库的分析接口，静态页面服务通过JS调用REST服务接口，并通过展示页面呈现。

## 1. 使用方法

analyser是基于Python的一个WEB服务，使用前需要先完成该服务的配置：

### 1.1 配置说明

analyser服务需要配置的内容在[config/__init__.py](config/__init__.py)文件中：

| 名称                  | 说明                                                         |
| --------------------- | ------------------------------------------------------------ |
| SERVER_IP             | 服务器的IP地址，默认为localhost                              |
| SERVER_PORT           | 服务器的端口，默认端口为9999                                 |
| HTML_PATH             | analyser/html的绝对路径，使用绝对路径可以保证无论在哪里启动analyser服务都可正常访问静态页面资源。 |
| PRODUCT_ROOT_PATH     | 产品的架构信息数据根目录：<br/>db目录：使用者需要在此目录下按照product_name/product_version/archinfo.db的方式归档架构信息数据库。<br/>graphviz：运行时analyser会自动创建此目录，用于缓存通过graphviz生成的图片资源。 |
| GRAPHVIZ_ENABLE_CACHE | 是否使能graphviz的cache功能；生产环境建议默认使能，否则会每次请求都执行graphviz来生成图片。 |

### 1.2 启动方法

analyser启动方法为：python2 analyser.py

> 依赖说明：
>
> analyser使用过程中依赖graphviz生成依赖关系图，需要确保服务器已安装。
>
> 	Ubuntu服务器的安装方法参考：sudo apt install graphviz
> 	macOS安装方法参考：brew install graphviz

启动后通过浏览器访问http://localhost:9999即可查看内容。

## 2. 代码结构说明

- html：提供静态页面，主要包括HTML、JavaScript和CSS等页面资源。
- loader：用于加载各个产品的架构信息数据库。
- rest：基于已加载的架构信息数据库提供REST接口供静态页面展示相应内容。