#! /usr/bin/env python
#coding=utf-8

SERVER_IP="127.0.0.1"
#SERVER_IP="7.190.75.123"
SERVER_PORT=9999

# static html files path
HTML_PATH="/Users/handy/Documents/work/projects/python/macArchInfo/analyser/html"


# Product root path include:
#   db: product architecture information db organized with product_name/product_version/symdb.db
#   graphviz: generating graphviz related files for caching
PRODUCT_ROOT_PATH="/Users/handy/Documents/work/projects/python/assets"

GRAPHVIZ_ENABLE_CACHE=False


REST_PREFIX="symdb"
