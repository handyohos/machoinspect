#! /usr/bin/env python
#coding=utf-8

import os

class BuilderContext(object):
	def __init__(self, graphviz, obj, xargs, f=None):
		self.graphviz = graphviz
		self.obj = obj
		self.xargs = xargs
		self.f = f

	def setFile(self, f):
		self.f = f

	def setObj(self, obj):
		self.obj = obj

class Builder(object):
	def getGraphVizInfo(self, context):
		return ""

	def generateObjectGraph(self, context):
		return ""
		if context.xargs["type"] not in ("deps_total", "dependedBy_total"):
			context.f.write("rankdir=\"LR\"\n")
		if context.xargs["type"] == "deps_total":
			self._generateAllDepsGraph(context.obj, context.xargs, context.f)
		elif context.xargs["type"] == "dependedBy_total":
			self._generateAllDependedByGraph(context.obj, context.xargs, context.f)
		else:
			self._generateContextGraph(context.obj, context.xargs, context.f)
