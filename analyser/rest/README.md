

# REST接口

## 1. REST接口总体说明

REST接口对外提供了所有的架构信息获取的方式，所有的REST接口默认都有前缀：**/symdb**。下表是当前支持的所有REST接口列表：

| 模块                                        | 说明                                     |
| ------------------------------------------- | ---------------------------------------- |
| /products                                   | 获取产品列表，包括产品名称以及版本信息。 |
| /{product}/{version}/modules                | 获取指定产品的模块信息列表。             |
| /{product}/{version}/modules/{id}/fields    | 获取指定模块的关键字段信息               |
| /{product}/{version}/deps                   | 获取指定产品的依赖信息列表。             |
| /{product}/{version}/deps/{id}/symbols      | 获取指定依赖的符号列表。                 |
| /{product}/{version}/components             | 获取指定产品的部件列表。                 |
| /{product}/{version}/components/{id}/deps   | 获取指定部件的依赖信息列表。             |
| /{product}/{version}/components/{id}/fields | 获取指定部件的关键字段信息。             |
| /{product}/{version}/processes              | 获取指定产品的进程内存信息列表。         |
| /{product}/{version}/processes/{id}/objects |                                          |
| /{product}/{version}/processes/{id}/vmas    |                                          |
| /{product}/{version}/objects                |                                          |
| /{product}/{version}/objects/{id}/processes |                                          |



## 2. products接口说明





