# MachOInspect

#### 介绍
macOS MachO相关的详细工具。

#### 软件架构
##### [collector](collector/README.md)

​	采集macOS的dylib符号信息以及内存信息。

##### [analyser](analyser/README.md)

​	通过WEB可视化展示采集的数据。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  
