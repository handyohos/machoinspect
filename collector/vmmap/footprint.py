#! /usr/bin/env python
#coding=utf-8

import os
import codecs

FOOTPRINT_SIZE_KEYS = ("Dirty", "Clean", "Reclaimable", "Regions")

class ProcessFootprint(dict):
    def __init__(self, filename):
        super().__init__()
        self.__load(filename)

    def get_all(self):
        return self.__categories

    def __unnatural_size(s, unit):
        s = s.strip()
        unit = unit.strip()
        if unit == "B":
            return int(s)
        if unit == "KB":
            return int(s) * 1024
        if unit == "MB":
            return int(s) * 1024 * 1024
        if unit == "GB":
            return int(s) * 1024 * 1024 * 1024
        return int(s)

    def __parse_one_line(self, line):
        parts = line.split()
        category = {}
        category["Dirty"] = ProcessFootprint.__unnatural_size(parts[0], parts[1])
        category["Clean"] = ProcessFootprint.__unnatural_size(parts[2], parts[3])
        category["Reclaimable"] = ProcessFootprint.__unnatural_size(parts[4], parts[5])
        category["Regions"] = int(parts[6])
        category["Category"] = " ".join(parts[7:])
        self.__categories.append(category)

    def getSum(self, prefix="footprint_"):
        res = {}
        for k in FOOTPRINT_SIZE_KEYS:
            res[prefix + k] = 0
        for category in self.__categories:
            for k in FOOTPRINT_SIZE_KEYS:
                res[prefix+k] = res[prefix+k] + category[k]
        return res

    def __load(self, filename):
        self.__categories = []
        if not os.path.exists(filename):
            return
        with codecs.open(filename, 'r', 'utf-8') as f:
            detected = False
            for line in f.readlines():
                line = line.strip()
                if not detected:
                    if line.startswith("---   "):
                        detected = True
                    continue
                if line.startswith("---   "):
                    break
                self.__parse_one_line(line)

    def report(self, fileName):
        f = open(fileName, "w")
        f.write("<categories>\n")
        for category in self.__categories:
            f.write("<category>\n")
            for k in FOOTPRINT_SIZE_KEYS + ("Category", ):
                f.write("<%s>%s</%s>\n" % (k, category[k], k))
            f.write("</category>\n")
        f.write("</categories>\n")
        f.close()

if __name__ == "__main__":
    import sys
    import traceback

    if len(sys.argv) <= 1:
        print("footprint.py footprint_file1 footprint_file2 ...")
        sys.exit()

    for footprint in sys.argv[1:]:
        try:
            print("Parsing footprint file: %s" % footprint)
            p = ProcessFootprint(footprint)
            print(p.get_all())
            out = os.path.splitext(footprint)[0] + "_footprint.xml"
            p.report(out)
            print("  parsed footprint file: %s" % out)
        except:
            traceback.print_exc()
