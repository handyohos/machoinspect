#### 介绍

[vm_stat.py](vm_stat.py)用于对vm_stat命令采集的macOS系统总体内存信息进行解析。

[footprint.py](footprint.py)用于对footprint命令采集的单个进程的汇总内存信息进行解析。

[vmmap.py](vmmap.py)用于对vmmap命令采集的单个进程的详细内存信息进行解析。vmmap命令可以详细列举每个进程包函的所有内存页（Region）信息。

[processes.py](processes.py)可完成内存采集功能：

- 单个进出的内存采集：指定pid，采集该pid的footprint和vmmap信息
- 系统全量进出的内存采集：不指定pid，通过ps查询所有进程，并对每个进出执行footprint和vmmap命令采集内存信息

#### 使用说明

```
usage: processes.py [-h] -o OUTPUT [-v] [-f] [-t] [-p PID]

Collect memory information from macOS.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        memory information output directory
  -v, --no-vmmap        Donot collect detail vmmap information for process
  -f, --no-footprint    Donot collect footprint information for process
  -t, --no-format       Donot generate formatted memory information
  -p PID, --pid PID     Update memory information
```

|      | 选项           | 参数                           | 必选 | 说明                                                         |
| ---- | -------------- | ------------------------------ | ---- | ------------------------------------------------------------ |
| -o   | --output       | 采集的原始内存数据文件存放目录 | 是   | 按照pid名称存放每个进程的内存信息：<br>ps.txt：ps命令返回的所有进程信息<br>vm_stat.txt：vm_stat命令获取的系统内存信息<br>pid.vmmap：进程的vmmap信息<br>pid.footprint：进程的footprint信息<br>formatted/ps.xml：每个进出格式化后的内存信息<br/>formatted/vm_stat.xml：格式化后的系统内存信息<br/>formatted/pid_vmmap.xml：格式化的vmmap内存信息<br>formatted/pid_footprint.xml：格式化的footprint内存信息 |
| -p   | --pid          | 采集指定进程的内存信息         | 否   | 未指定pid时，采集系统所有进程的内存信息                      |
| -v   | --no-vmmap     | 不采集vmmap信息                | 否   |                                                              |
| -f   | --no-footprint | 不采集footprint信息            | 否   |                                                              |
| -t   | --no-format    | 不生成格式化的内存信息         | 否   |                                                              |


