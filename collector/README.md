#### 介绍

[collector.py](collector.py)可完成以下两大部分功能：

- 实现对macOS系统基础库的符号以及依赖关系分析与采集（系统库解压方法参考下文）
- 对[内存采集器](vmmap/README.md)采集的内存进行结构化分析

#### 使用说明

```
usage: collector.py [-h] -i INPUT [-o OUTPUT] [-s] [-e] [-m MEM] [-l] [-d] [-p IP] [-n NAME]

Collect architecture information from asset files.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        input asset files root directory
  -o OUTPUT, --output OUTPUT
                        output architecture information database directory
  -s, --scan            Update memory information only
  -e, --none            Update nonthing, scan elf files only
  -m MEM, --mem MEM     Update memory information
  -l, --last            Update last step module summary information only
  -d, --depends         Update module summary information only
  -p IP, --ip IP        Uploading IP address
  -n NAME, --name NAME  Build product name
```

| 选项     | 参数                                                | 必选 | 说明                                                         |
| -------- | --------------------------------------------------- | ---- | ------------------------------------------------------------ |
| --input  | 解压的macOS基础库路径                               | 是   |                                                              |
| --output | 生成的数据库路径                                    | 否   | 如果没有指定，则自动生成到--input指定的目录；<br>生成的数据库文件名称为archinfo.db。 |
| --mem    | [内存采集器](vmmap/README.md)采集的原始内存数据文件 | 否   | 如果指定此参数，则对--output目录下的archinfo.db追加结构化的内存数据。 |



#### macOS系统基础库解压方法

macOS系统基础库都统一合并到/System/Library/dyld/目录下了，可以通过[dyld-shared-cache-extractor](https://github.com/keith/dyld-shared-cache-extractor)解压出所有的系统库：

```sh
# 安装，没法直接安装，建议编译生产二进制
brew install keith/formulae/dyld-shared-cache-extractor

# 源码编译
cmake -B build
cmake --build build
cmake --install build

# 解压
DEVELOPER_DIR=/Applications/Xcode.app  ./build/dyld-shared-cache-extractor /System/Library/dyld/dyld_shared_cache_x86_64 tmp
```

